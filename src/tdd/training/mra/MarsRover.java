package tdd.training.mra;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class MarsRover {
	
	private enum Direction {NORTH, EAST, SOUTH, WEST};
	private boolean[][] positions;
	private int roverX, roverY;
	private Direction dir;

	/**
	 * It initializes the rover at the coordinates (0,0), facing North, on a planet
	 * (represented as a grid with x and y coordinates) containing obstacles.
	 * 
	 * @param planetX         The x dimension of the planet.
	 * @param planetY         The y dimension of the planet.
	 * @param planetObstacles The obstacles on the planet. Each obstacle is a string
	 *                        (without white spaces) formatted as follows:
	 *                        "(oi_x,oi_y)". <code>null</code> if the planet does
	 *                        not contain obstacles.
	 * 
	 * @throws MarsRoverException
	 */
	public MarsRover(int planetX, int planetY, List<String> planetObstacles) throws MarsRoverException {
		this.roverX = 0;
		this.roverY = 0;
		this.dir = Direction.NORTH;
		this.positions = new boolean[planetX][planetY];
		
		int x, y;
		for(String obs : planetObstacles) {
			x = Character.getNumericValue(obs.charAt(1));
			y = Character.getNumericValue(obs.charAt(3));
			positions[x][y] = true;
		}
	}

	/**
	 * It returns whether, or not, the planet (where the rover moves) contains an
	 * obstacle in a cell.
	 * 
	 * @param x The x coordinate of the cell
	 * @param y The y coordinate of the cell
	 * @return <true> if the cell contains an obstacle, <false> otherwise.
	 * @throws MarsRoverException
	 */
	public boolean planetContainsObstacleAt(int x, int y) throws MarsRoverException {
		return positions[x][y];
	}

	/**
	 * It lets the rover move on the planet according to a command string. The
	 * return string contains the new position of the rover, its direction, and the
	 * obstacles it has encountered while moving on the planet (if any).
	 * 
	 * @param commandString A string that can contain a single command -- i.e. "f"
	 *                      (forward), "b" (backward), "l" (left), or "r" (right) --
	 *                      or a combination of single commands.
	 * @return The return string that contains the position and direction of the
	 *         rover, and the obstacles the rover has encountered while moving on
	 *         the planet (if any). The return string (without white spaces) has the
	 *         following format: "(x,y,dir)(o1_x,o1_y)(o2_x,o2_y)...(on_x,on_y)". x
	 *         and y define the new position of the rover while dir represents its
	 *         direction (i.e., N, S, W, or E). Finally, oi_x and oi_y are the
	 *         coordinates of the i-th encountered obstacle.
	 * @throws MarsRoverException
	 */
	public String executeCommand(String commandString) throws MarsRoverException {
		Set<String> obsEncountered = new LinkedHashSet<String>();
		for (Character command : commandString.toCharArray()) {
			switch(command) {
			case 'r': this.turn(1);  break;
			case 'l': this.turn(-1); break;
			case 'f': this.move(1, obsEncountered);  break;
			case 'b': this.move(-1, obsEncountered); break;
			}
		}
		
		String state = "(" + roverX + "," + roverY + "," + dir.name().charAt(0) + ")";
		for(String obs : obsEncountered) 
			state += obs;
		return state;
	}
	
	/**
	 * Set the new direction based on the rotation integer passed as argument.
	 * 
	 * @param rotation An integer that represent the rotation. 1 stands for 
	 *                 "right", -1 for "left"
	 */
	public void turn(int rotation) {
		rotation = (dir.ordinal() + rotation) % 4;
		if(rotation < 0) rotation += 4;
		dir = Direction.values()[rotation];
	}
	
	/**
	 * Makes a single step on the planet.
	 * 
	 * @param step An integer representing how to move. 1 stands for "forward", 
	 *             -1 stands for "backward".
	 * @param obsEncountered A set containing prevoiusly encountered obstacles
	 * @throws MarsRoverException
	 */
	public void move(int step, Set<String> obsEncountered) throws MarsRoverException {

		switch(dir) {
		case SOUTH:
		case WEST: step *= -1;
		default: break; 
		}
		
		switch (dir) {
		case SOUTH:
		case NORTH:
			int newRoverY = (roverY + step) % positions.length;
			if(newRoverY < 0) 
				newRoverY += positions.length;
			if(this.planetContainsObstacleAt(roverX, newRoverY))
				obsEncountered.add("(" + roverX + "," + newRoverY +")"); 
			else 
				roverY = newRoverY;
			break;
		case WEST:
		case EAST:
			int newRoverX = (roverX + step) % positions[0].length;
			if(newRoverX < 0) 
				newRoverX += positions[0].length;
			if(this.planetContainsObstacleAt(newRoverX, roverY))
				obsEncountered.add("(" + newRoverX + "," + roverY +")"); 
			else 
				this.roverX = newRoverX;
			break;
		}
	}
	
	/**
	 * Set the new position of the rover on the planet
	 * @param state A string formatted as (x,y,D) where x and y are integers and 
	 * 		  D is one of the four directions.
	 * @throws MarsRoverException
	 */
	public void setRoverState(String state) throws MarsRoverException { 
		int x = Character.getNumericValue(state.charAt(1));
		int y = Character.getNumericValue(state.charAt(3));
		Character d = state.charAt(5);
		
		if (this.planetContainsObstacleAt(x, y))
			throw new MarsRoverException("This position has an obstacles");
		
		if(x < 0 || x > positions[0].length-1) 
			throw new MarsRoverException("Respect planet boundaries, x between [0, " + (positions[0].length-1) + "]");
		if(y < 0 || y > positions.length-1) 
			throw new MarsRoverException("Respect planet boundaries, y between [0, " + (positions.length-1) + "]");
		switch(d) {
		case 'N': dir = Direction.NORTH; break;
		case 'S': dir = Direction.SOUTH; break;
		case 'E': dir = Direction.EAST;  break;
		case 'W': dir = Direction.WEST;  break;
		default:
			throw new MarsRoverException("Invalid direction: " + d);
		}
		
		this.roverX = x;
		this.roverY = y;
	}
	
}
