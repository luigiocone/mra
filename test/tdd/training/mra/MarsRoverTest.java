package tdd.training.mra;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class MarsRoverTest {
	
	private MarsRover mr;
	
	@Before
	public void setUp() throws MarsRoverException {
		List<String> obstacles = new ArrayList<String>();
		obstacles.add("(4,7)");
		mr = new MarsRover(10, 10, obstacles);
	}

	@Test
	// user story #1
	public void testPlanetContainsObstacleAt() throws MarsRoverException {
		assertTrue(mr.planetContainsObstacleAt(4, 7));
		assertFalse(mr.planetContainsObstacleAt(1, 1));
	}
	
	@Test
	// user story #2
	public void testExecuteCommandEmptyCommand() throws MarsRoverException {
		assertEquals("(0,0,N)", mr.executeCommand(""));
	}
	
	@Test
	// user story #3
	public void testExecuteCommandLeft() throws MarsRoverException {
		assertEquals("(0,0,W)", mr.executeCommand("l"));
	}
	
	@Test
	// user story #3
	public void testExecuteCommandRight() throws MarsRoverException {
		assertEquals("(0,0,E)", mr.executeCommand("r"));
	}
	
	@Test
	// user story #4
	public void testExecuteCommandForward() throws MarsRoverException {
		assertEquals("(0,1,N)", mr.executeCommand("f"));
	}
	
	@Test
	// custom method
	public void testSetState() throws MarsRoverException {
		mr.setRoverState("(5,8,E)");
		assertEquals("(5,8,E)", mr.executeCommand(""));
	}
	
	@Test(expected = MarsRoverException.class)
	// custom method
	public void testSetStateOnObstacles() throws MarsRoverException {
		mr.setRoverState("(4,7,E)");
	}
	
	@Test
	// user story #5
	public void testExecuteCommandBackward() throws MarsRoverException {
		mr.setRoverState("(5,8,E)");
		assertEquals("(4,8,E)", mr.executeCommand("b"));
	}
	
	@Test
	// user story #6
	public void testExecuteCombinedMoving() throws MarsRoverException {
		assertEquals("(2,2,E)", mr.executeCommand("ffrff"));
	}
	
	@Test
	// user story #7
	public void testExecuteCommandBackwardOverBoundaries() throws MarsRoverException {
		assertEquals("(0,9,N)", mr.executeCommand("b"));
	}
	
	@Test
	// user story #7
	public void testExecuteCommandForwardOverBoundaries() throws MarsRoverException {
		mr.setRoverState("(0,9,N)");
		assertEquals("(0,0,N)", mr.executeCommand("f"));
	}
	
	@Test
	// user story #8
	public void testExecuteCommandSingleObstacleEncountered() throws MarsRoverException {
		List<String> obstacles = new ArrayList<String>();
		obstacles.add("(2,2)");
		mr = new MarsRover(10, 10, obstacles);
		assertEquals("(1,2,E)(2,2)", mr.executeCommand("ffrfff"));
	}
	
	@Test
	// user story #9
	public void testExecuteCommandMultipleObstaclesEncountered() throws MarsRoverException {
		List<String> obstacles = new ArrayList<String>();
		obstacles.add("(2,2)");
		obstacles.add("(2,1)");
		mr = new MarsRover(10, 10, obstacles);
		assertEquals("(1,1,E)(2,2)(2,1)", mr.executeCommand("ffrfffrflf"));
	}
	
	@Test
	// user story #10
	public void testExecuteCommandSingleObstacleAtBoundaryEncountered() throws MarsRoverException {
		List<String> obstacles = new ArrayList<String>();
		obstacles.add("(0,9)");
		mr = new MarsRover(10, 10, obstacles);
		assertEquals("(0,0,N)(0,9)", mr.executeCommand("b"));
	}
	
	@Test
	// user story #11
	public void testExecuteCommandTourAroundThePlanet() throws MarsRoverException {
		List<String> obstacles = new ArrayList<String>();
		obstacles.add("(2,2)");
		obstacles.add("(0,5)");
		obstacles.add("(5,0)");
		mr = new MarsRover(6, 6, obstacles);
		assertEquals("(0,0,N)(2,2)(0,5)(5,0)", mr.executeCommand("ffrfffrbbblllfrfrbbl"));
	}
}
